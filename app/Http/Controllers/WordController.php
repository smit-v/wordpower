<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreateWordRequest;
use App\Models\Word;
use Illuminate\Http\Request;

class WordController extends Controller
{
    public function setRange(Request $request)
    {
        $validated = request()->validate([
            'From-field-test' => 'required|numeric|gt:0',
            'To-field-test' => 'required|numeric|gt:0|gt:From-field-test',
            'type' => 'required'
        ]);
        $from = $validated['From-field-test'];
        $to = $validated['To-field-test'];
        if($request->type == 1)
            return redirect(route('words.test',[$from,$to]));
        elseif($request->type == 2)
            return redirect(route('words.check',[$from,$to]));
        elseif($request->type == 3)
            return redirect(route('words.mcq',[$from,$to]));
        elseif($request->type == 4)
            return redirect(route('words.mcqPage',[$from,$to]));
    }

    private function makeTest($request)
    {
        $checks = [$request->from,$request->to];
        $questions = Word::inRandomOrder()->whereBetween('id',$checks)->get();
        $question_paper = [];
        foreach($questions as $question)
        {
            $Otherwords = Word::inRandomOrder()->limit(3)->get();
            $meanings[] = [$question->meaning,$question->id];
            for($i=0; $i<3; $i++)
            {
                $meanings[] = [$Otherwords[$i]->meaning, $Otherwords[$i]->id];
            }
            shuffle($meanings);
            $meanings[] = $question->id;
            $question_paper[$question->name] = $meanings;
            $meanings = [];
        }
        return [$question_paper, $checks];
    }

    public function MCQ(Request $request)
    {
        $vals = $this->makeTest($request);
        return view('mcq',['question_paper' => $vals[0],'checks' => $vals[1]]);
    }

    public function MCQpage(Request $request)
    {
        $vals = $this->makeTest($request);
        return view('mcqPage',['question_paper' => $vals[0],'checks' => $vals[1]]);
    }

    public function makeTestQuestion(Request $request)
    {
        $checks = [$request->from,$request->to];
        $question = Word::inRandomOrder()->whereBetween('id',$checks)->first();
        $word = [$question->name, $question->id];
        $Otherwords = Word::inRandomOrder()->limit(3)->get();
        $meanings[] = [$question->meaning,$question->id];
        for($i=0; $i<3; $i++)
        {
            $meanings[] = [$Otherwords[$i]->meaning, $Otherwords[$i]->id];
        }
        shuffle($meanings);
        return view('test',[
            'word' => $word,
            'meanings' => $meanings,
            'checks' => $checks
        ]);
    }

    public function index()
    {
        $words = Word::search()->paginate(4);
        return view('index',['words'=> $words]);
    }

    public function rangeCheck()
    {

        $validated = request()->validate([
            'From-field' => 'required|numeric|gt:0',
            'To-field' => 'required|numeric|gt:0|gt:From-field',
        ]);
        $checks = array($validated['From-field'],$validated['To-field']);
        // dd($validated['From-field']);
        $words = Word::whereBetween('id',$checks)->get();
        return view('range',['words'=> $words]);
    }

    public function create()
    {
        return view('create');
    }

    public function store(CreateWordRequest $request)
    {

        Word::create([
            'name' => $request->name,
            'meaning' => $request->meaning
        ]);

        return redirect('/words/create/?q=success&op=add');
    }

    public function edit(int $id)
    {
        $word = Word::findOrFail($id);
        return view('edit',['word'=>$word]);
    }

    public function update(int $id)
    {
        $old = Word::findOrFail($id);
        $word = request()->validate([
            'name' => 'required',
            'meaning' => 'required'
        ]);
        $old->update($word);
        return redirect("/words/$old->id/edit/?q=success&op=edit");
    }

    public function checkWord(Request $request)
    {
        $checks = [$request->from,$request->to];
        $question = Word::inRandomOrder()->whereBetween('id',$checks)->first();
        return view('check',['word' => $question,'check' => $checks]);
    }
}
