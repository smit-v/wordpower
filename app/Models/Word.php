<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Word extends Model
{
    use HasFactory;
    protected $guarded = [''];

    public function scopeSearch($query)
    {
        $search = request('search');
        if($search)
        {
            return $query->where('name','like',"%$search%");
        }
        return $query;
    }
}
