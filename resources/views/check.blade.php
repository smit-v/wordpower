@extends('layouts.app')
@section('content')


<div class="mt-3">
    <div class="card p-0">
        <div class="card-header bg-dark text-light">
          <p class="text-center font-weight-bold m-0">Word {{ $word->id }}</p>
        </div>
        <div class="card-body">
          <h1 class=" text-center card-title text-capitalize">{{ $word->name }}</h1>
          <p id="meaning" class="text-center card-text text-capitalize mt-3" style="display:none;">{{ $word->meaning }}</p>
          <div class="d-flex justify-content-center mt-5">
            <a id="btn-show" class="btn btn-primary text-light">Show</a>
            <a href="{{ route('words.check',$check) }}" class="btn btn-secondary ml-3 text-light">Next</a>
          </div>
        </div>
      </div>
</div>
@endsection

@section('footer-val','fixed-bottom')
@section('scripts')
<script>

    $('#btn-show').click(function(){
        $('#meaning').fadeIn("slow");
    });
</script>
@endsection
