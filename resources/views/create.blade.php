@extends('layouts.app')
@section('content')


    <div class="d-flex justify-content-center">
        <div class="card mt-5 w-75">
            <div class="card-header bg-dark text-light">
              <p class="font-weight-bold m-0">Add a new word</p>
            </div>
            <div class="card-body">

                <form action={{ route('words.store') }} method="POST">
                    @csrf
                    <div class="form-group">
                      <label for="name">Word</label>
                      <input type="text" name="name" class="form-control" id="name" value="{{ old('name') }}">
                      @error('name')
                          <small class="text-danger">{{ $message }}</small>
                      @enderror
                    </div>
                    <div class="form-group">
                      <label for="meaning">Word Meaning</label>
                      <input type="text" name="meaning" class="form-control" id="meaning" value="{{ old('meaning') }}">
                      @error('meaning')
                          <small class="text-danger">{{ $message }}</small>
                      @enderror
                    </div>

                    <button type="submit" class="btn btn-primary">Submit</button>
                    <a id="clear" class="btn bg-secondary text-light">Clear</a>
                  </form>

            </div>
          </div>
    </div>
    <div class="d-flex justify-content-center">
        <div class="card mt-3" id="success_card">
            <div class="card-body">
              <p class="text-success m-0">The word has beed added!</p>
        </div>
    </div>
    </div>


@endsection

@section('footer-val','fixed-bottom')
@section('scripts')
<script>
    $('#success_card').hide();
    <?php
    $q = "";
    $op = "";

    if(isset($_GET['q']))
    {
        $q = $_GET['q'];
    }
    if(isset($_GET['op']))
    {
        $op = $_GET['op'];
    }

    if($q == "success" && $op == "add")
    {
    ?>


            $('#success_card').show();

        setTimeout(function(){
            $('#success_card').hide();

        }, 2000);



    <?php
    }
    ?>
        </script>




<script>
    $('#clear').click(function(){
        $('#name').val(" ");
        $('#meaning').val(" ");
    });
</script>
@endsection

