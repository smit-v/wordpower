@extends('layouts.app')
@section('content')


    <div class="d-flex justify-content-center">
        <div class="card mt-5 w-75">
            <div class="card-header bg-dark text-light">
              <p class="font-weight-bold m-0">Edit word</p>
            </div>
            <div class="card-body">

                <form action={{ route('words.update',$word->id) }} method="POST">
                    @csrf
                    @method('PUT')
                    <div class="form-group">
                      <label for="name">Word</label>
                      <input type="text" name="name" class="form-control" id="name" value="{{ $word->name }}">
                      @error('name')
                          <small class="text-danger">{{ $message }}</small>
                      @enderror
                    </div>
                    <div class="form-group">
                      <label for="meaning">Word Meaning</label>
                      <input type="text" name="meaning" class="form-control" id="meaning" value="{{ $word->meaning }}">
                      @error('meaning')
                          <small class="text-danger">{{ $message }}</small>
                      @enderror
                    </div>

                    <button id="btn-edit" type="submit" class="btn btn-primary">Edit</button>
                    <a id="clear" class="btn bg-secondary text-light">Clear</a>
                  </form>

            </div>
          </div>
    </div>
    <div class="d-flex justify-content-center">
        <div class="card mt-3" id="success_card">
            <div class="card-body">
              <p class="text-warning m-0">The word has beed edited!</p>
        </div>
    </div>
    </div>


@endsection

@section('footer-val','fixed-bottom')
@section('scripts')
<script>
    $('#btn-edit').hide();
    $('#name').keypress(function(){
        $('#btn-edit').show();
    });
    $('#meaning').keypress(function(){
        $('#btn-edit').show();
    });
</script>
<script>
    $('#success_card').hide();
    <?php
    $q = "";
    $op = "";

    if(isset($_GET['q']))
    {
        $q = $_GET['q'];
    }
    if(isset($_GET['op']))
    {
        $op = $_GET['op'];
    }

    if($q == "success" && $op == "edit")
    {
    ?>


            $('#success_card').show();

        setTimeout(function(){
            $('#success_card').hide();

        }, 3000);



    <?php
    }
    ?>
        </script>




<script>
    $('#clear').click(function(){
        $('#name').val(" ");
        $('#meaning').val(" ");
    });
</script>
@endsection

