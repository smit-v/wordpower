    @include('layouts.navbar')

    <div class="container">
        @yield('content')
    </div>

    @include('layouts.modals')

    @include('layouts.footer')

