<!-- Modal -->
<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="exampleModalLabel">Range for MCQs Check</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <form  action="{{ route('words.test.setRange')}}"  method="POST">
            @csrf
            <div class="modal-body row">
                <div class="form-group col-6 mb-2 ">
                    <input type="number" class="form-control" id="from" name='From-field-test' placeholder="From" value="1">
                </div>
                <div class="form-group col-6 mb-2">
                  <input type="number" class="form-control" id="to" name='To-field-test' placeholder="To">
                </div>
                <input type="hidden" value="1" name="type">
            </div>

            <div class="modal-footer">
            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
            <button type="submit" class="btn btn-success" id="startTestbtn">Start Check</button>
            </div>
        </form>
      </div>
    </div>
</div>

<!-- Modal -->
<div class="modal fade" id="exampleModal2" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel2" aria-hidden="true">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="exampleModalLabel2">Range for Show word Check</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <form  action="{{ route('words.test.setRange')}}"  method="POST">
            @csrf
            <div class="modal-body row">
                <div class="form-group col-6 mb-2 ">
                    <input type="number" class="form-control" id="from" name='From-field-test' placeholder="From" value="1">
                </div>
                <div class="form-group col-6 mb-2">
                  <input type="number" class="form-control" id="to" name='To-field-test' placeholder="To">
                </div>
                <input type="hidden" value="2" name="type">
            </div>

            <div class="modal-footer">
            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
            <button type="submit" class="btn btn-success" id="startTestbtn">Start Check</button>
            </div>
        </form>
      </div>
    </div>
</div>


<div class="modal fade" id="exampleModal3" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel3" aria-hidden="true">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="exampleModalLabel3">Range for MCQs test</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <form  action="{{ route('words.test.setRange')}}"  method="POST">
            @csrf
            <div class="modal-body row">
                <div class="form-group col-6 mb-2 ">
                    <input type="number" class="form-control" id="from" name='From-field-test' placeholder="From" value="1">
                </div>
                <div class="form-group col-6 mb-2">
                  <input type="number" class="form-control" id="to" name='To-field-test' placeholder="To">
                </div>
                <input type="hidden" value="3" name="type">
            </div>

            <div class="modal-footer">
            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
            <button type="submit" class="btn btn-success" id="startTestbtn">Start Test</button>
            </div>
        </form>
      </div>
    </div>
</div>

<div class="modal fade" id="exampleModal4" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel4" aria-hidden="true">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="exampleModalLabel4">Range for Show word Test</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <form  action="{{ route('words.test.setRange')}}"  method="POST">
            @csrf
            <div class="modal-body row">
                <div class="form-group col-6 mb-2 ">
                    <input type="number" class="form-control" id="from" name='From-field-test' placeholder="From" value="1">
                </div>
                <div class="form-group col-6 mb-2">
                  <input type="number" class="form-control" id="to" name='To-field-test' placeholder="To">
                </div>
                <input type="hidden" value="4" name="type">
            </div>

            <div class="modal-footer">
            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
            <button type="submit" class="btn btn-success" id="startTestbtn">Start Test</button>
            </div>
        </form>
      </div>
    </div>
</div>
