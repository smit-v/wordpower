<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
    <title>Word Power</title>
</head>
<body>
{{-- Navbar --}}
<nav class="navbar navbar-expand-lg navbar-dark bg-dark">
    <a class="navbar-brand text-light">Word Power</a>
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
      <span class="navbar-toggler-icon"></span>
    </button>

    <div class="collapse navbar-collapse" id="navbarSupportedContent">
      <ul class="navbar-nav mr-auto">
        <li class="nav-item {{ request()->path() === 'words' ? 'active' : '' }}">
          <a class="nav-link" href="{{ route('words.index') }}">Home</a>
        </li>
        {{-- <li class="nav-item  {{ request()->path() === 'test' ? 'active' : '' }}"> --}}
          {{-- <a class="nav-link" href="{{ route('words.test') }}">Take test</a> --}}
          {{-- <a class="nav-link"  data-toggle="modal" data-target="#exampleModal">Take test</a> --}}
        {{-- </li> --}}
        {{-- <li class="nav-item  {{ request()->path() === 'check' ? 'active' : '' }}"> --}}
            {{-- <a class="nav-link" href="{{ route('words.check') }}">Check Word</a> --}}
            {{-- <a class="nav-link"  data-toggle="modal" data-target="#exampleModal2">Check Word</a> --}}
          {{-- </li> --}}

          <li class="nav-item dropdown">
            <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
              Random Word Checks
            </a>
            <div class="dropdown-menu" aria-labelledby="navbarDropdown">

              <a class="dropdown-item"  data-toggle="modal" data-target="#exampleModal">MCQs Check</a>
              <a class="dropdown-item"  data-toggle="modal" data-target="#exampleModal2">Show Word Check</a>

            </div>
          </li>
          <li class="nav-item dropdown">
            <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
              Tests
            </a>
            <div class="dropdown-menu" aria-labelledby="navbarDropdown">

              <a class="dropdown-item"  data-toggle="modal" data-target="#exampleModal3">MCQs Test</a>
              <a class="dropdown-item"  data-toggle="modal" data-target="#exampleModal4">Show Word Test</a>

            </div>
          </li>
        <li class="nav-item">
            <a href="{{ route('words.create') }}" class="nav-link text-warning {{ request()->path() === 'words/create' ? 'font-weight-bold' : '' }}">Add New Word</a>
        </li>

      </ul>

    </div>
    <form action="{{ route('words.index') }}" class="form-inline my-2 my-lg-0" method="GET">
        @csrf
        <input class="form-control mr-sm-2" name="search" type="search" placeholder="Search any word" aria-label="Search" value="{{ request('search') }}">
        <button class="btn btn-outline-success my-2 my-sm-0" type="submit">Search</button>
      </form>
  </nav>
{{-- /Navbar --}}
