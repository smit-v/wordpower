@extends('layouts.app')
@section('content')

@foreach ($question_paper as $key=>$value)
    <div class="d-flex justify-content-center">

        <div class="card mt-5 w-75">
            <div class="card-header bg-dark text-light">
              <p class="font-weight-bold m-0">{{$loop->index + 1}}</p>
            </div>
            <div class="card-body">


                    <fieldset class="form-group">
                      <div class="row">
                        <div class="col-md-5">
                            <input id="word-id" type="hidden" value="{{ $value[4] }}">
                            <h1 class="text-capitalize">{{ $key }}</h1>
                        </div>
                        <div class="col-sm-7">
                          @for ($m=0; $m<4; $m++)
                          <div class="form-check">
                            <input id="radio{{$loop->index . $m}}" class="form-check-input" type="radio" name="radioans{{$loop->index}}" value="{{ $value[$m][1] }}">
                            <label class="form-check-label" for="radio{{$loop->index . $m}}">

                              <p class="text-capitalize">{{ $value[$m][0] }}</p>
                            </label>
                          </div>

                          @endfor
                          <div class="col-sm-8">
                            <p id="ans{{$loop->index}}" class="m-0"></p>
                          </div>
                        </div>
                      </div>
                    </fieldset>
            </div>
          </div>

    </div>
    @endforeach

      <div class="mt-5 d-flex justify-content-center align-content-center">
        <button id="btn-submit" class="btn btn-primary m-0">Submit</button>
      </div>
      <p id="page-ans" class="mt-3 text-center"></p>

@endsection

@section('footer-val','mt-3')
@section('scripts')
<script>
  ansPaper = [];
  @foreach ($question_paper as $key=>$value)
    ansPaper.push("{{ $value[4] }}");
  @endforeach

  flag=true;
  len_of_questionPaper = <?php echo (sizeof($question_paper))?>;
  $("#btn-submit").click(function(){
    var ans = $("input[name^='radioans']:checked");
    if(ans.length != len_of_questionPaper)
    {
        alert('Please answer all the questions');
            return;
    }
    else
    {
        for(i=0; i<len_of_questionPaper; i++)
        {
            if(ansPaper[i] == ans[i].value)
            {
                // console.log(i + " is right");
                $("#ans"+i).text("Yes!! Its Correct!!");
                $('#ans'+i).removeClass("text-danger");
                $('#ans'+i).addClass("text-success");
            }
            else
            {
                // console.log(i + " is wrong");
                flag=false;
                $('#ans'+i).text("Oh No! Its Wrong");
                $('#ans'+i).removeClass("text-success");
                $('#ans'+i).addClass("text-danger");
            }
        }
        if(!flag)
        {
            $("#page-ans").text("Something is wrong!");
            $('#page-ans').removeClass("text-success");
            $("#page-ans").addClass("text-danger");
        }
        else
        {
            $("#page-ans").text("Everything is right!");
            $('#page-ans').addClass("text-success");
            $("#page-ans").removeClass("text-danger");
        }
        flag=true;
        // console.log('Here');
    }

  });
</script>
@endsection
