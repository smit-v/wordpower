@extends('layouts.app')
@section('content')
    <div id="card-set" class="d-flex justify-content-center ">
        <div class="card mt-5 w-75">
            <div class="card-header bg-dark text-light">
              <p class="font-weight-bold m-0">MCQs</p>
            </div>
            <div class="card-body">
                <fieldset class="form-group">
                      <div class="row">
                        <div class="col-md-5">
                            <input id="word-id" type="hidden" value="">
                            <h1 id="show-word" class="text-capitalize"></h1>
                        </div>
                        <div class="col-sm-7">

                          @for($j=0; $j<4; $j++)
                          <div class="form-check">
                            <input id="radio{{$j}}" class="form-check-input" type="radio" name="radioans" value="">
                            <label class="form-check-label" for="radio{{$j}}">
                                <p id="show-option{{$j}}" class="text-capitalize"></p>
                            </label>
                          </div>
                          @endfor

                        </div>
                      </div>
                </fieldset>
            </div>
          </div>
    </div>


    <div id="controls-set">
        <div class="mt-5 d-flex justify-content-center align-content-center">
            <button id="btn-prev" onclick="prevQuestion()" class="btn btn-dark mr-2">Previous</button>
            <button id="btn-submit" onclick="saveAns()" class="btn btn-primary mr-2">Submit</button>
            <button id="btn-clear" onclick="clearAns()" class="btn btn-primary mr-2">Clear</button>
            <button id="btn-next" onclick="nextQuestion()" class="btn btn-dark m-0">Next</button>
          </div>
          <p id="page-ans" class="mt-3 text-center"></p>
          <div class="text-center">
              @foreach ($question_paper as $question)
                <button class="btn btn-warning ml-2" id="navQuestion{{ $loop->index }}" onclick="navQuestion({{ $loop->index }})"></button>
              @endforeach
          </div>
          <div class="text-center">
              <button id="btn-submit-Test" onclick="submitTest()" class="btn btn-success mt-5">Submit Test</button>
          </div>
    </div>

@endsection

@section('footer-val','mt-5')



@section('scripts')
<script>

    question_words = [];
    question_options = [];
    ansPaper = [];


    @foreach ($question_paper as $key=>$val)
        question_words.push("{{ $key }}");

        options = [];
        @for($i=0; $i<4; $i++)
            options.push(["{{ $val[$i][0] }}",{{ $val[$i][1] }}]);
            @if($val[4] == $val[$i][1])
                ans = [{{$val[4]}}, "{{ $val[$i][0] }}"];
                ansPaper.push(ans);
            @endif
        @endfor

        question_options.push(options);

    @endforeach

    savedAns = new Array(ansPaper.length).fill(0);
    currentofQuestions = -1;
    showQuestion(1);

    function nextQuestion()
    {
        showQuestion(1);
        checkSelectedAns();
    }

    function prevQuestion()
    {
        showQuestion(-1);
        checkSelectedAns();
    }

    function saveAns()
    {
        var ans = $("input[name='radioans']:checked").val();
        if(ans == null)
        {
            alert('Please select an option');
            return;
        }
        savedAns[currentofQuestions] = ans;
        $("#navQuestion"+currentofQuestions).addClass('btn-success');
        $("#navQuestion"+currentofQuestions).removeClass('btn-warning');
    }

    function clearAns()
    {
        $("input[name=radioans]:checked").prop('checked', false);
        savedAns[currentofQuestions] = 0;
        $("#navQuestion"+currentofQuestions).removeClass('btn-success');
        $("#navQuestion"+currentofQuestions).addClass('btn-warning');
    }

    function navQuestion(n)
    {
        currentofQuestions = n-1;
        showQuestion(1);
        checkSelectedAns();
    }

    function submitTest()
    {
        notAnswered = 0;
        rightAnswered = 0;
        wrongAnswered = 0;
        for(i=0; i<savedAns.length; i++)
        {
            if(savedAns[i] != 0)
            {
                if(savedAns[i] == ansPaper[i][0])
                {
                    rightAnswered++;
                }
                else
                {
                    wrongAnswered++;
                }
            }
            else
            {
                notAnswered++;
            }
        }
        settingHTML = `

            <div class="card mt-5 w-100">
                <div class="card-header bg-dark text-light">
                    <p class="font-weight-bold m-0">Results</p>
                </div>

                <div class="card-body">
                    <div class="d-flex">
                        <div class="col-4 text-center">
                            <h2 class="text-success">Correct</h2>
                            <h2 class="text-success display-3">`+ rightAnswered +`</h2>
                        </div>
                        <div class="col-4 text-center">
                            <h2 class="text-warning">Not Answered</h2>
                            <h2 class="text-warning display-3">`+ notAnswered +`</h2>
                        </div>
                        <div class="col-4 text-center">
                            <h2 class="text-danger">Wrong</h2>
                            <h2 class="text-danger display-3">`+ wrongAnswered +`</h2>
                        </div>
                    </div>
                    <div class="d-flex mt-5">
                        <table class="table table-hover">
                        <thead class="thead">
                            <tr>
                            <th scope="col">#</th>
                            <th scope="col">Word</th>
                            <th scope="col">Your Selection</th>
                            <th scope="col">Answer</th>
                            </tr>
                        </thead>
                        <tbody>


        `;

        tablesHTML = ``;



        for(i=0; i<ansPaper.length; i++)
        {
            wordnumber = i+1;
            word_check = question_words[i];
            correct_check = ansPaper[i][1];
            ans = savedAns[i];
            console.log(question_options[i]);
            index = question_options[i].flat().indexOf(parseInt(savedAns[i]))-1;
            if(index >= 0)
            {
                your_check = question_options[i].flat()[index];
                if(your_check == correct_check)
                {
                    your_check = `<td class="text-success">`+ question_options[i].flat()[index] +`</td>`;
                }
                else
                {
                    your_check = `<td class="text-danger">`+ question_options[i].flat()[index] +`</td>`;
                }
            }
            else
            {
                your_check = `<td class="text-warning"> Not Answered </td>`;
            }
            tablesHTML = tablesHTML + `
                        <tr>
                            <th scope="row">`+ wordnumber +`</th>
                            <th>`+ word_check +`</th>`
                            + your_check +

                            `<td class="text-success">`+ correct_check +`</td>
                        </tr>
            `;
        }

        settingHTML  = settingHTML+ tablesHTML +`
                                </tbody>
                        </table>
            </div>
            </div>
        </div>
        `;


        controlbtn = `<div class="d-flex justify-content-center">
                        <a class="btn btn-primary mt-5" href={{ route('words.index') }}>Back to Home</a>
                    </div>`;
        $("#controls-set").html(controlbtn);
        $("#card-set").html(settingHTML);
    }

    function showQuestion(val)
    {
        if(val == 1)
        {

            if(question_words.length-1 <= currentofQuestions)
            {
                return;
            }
            currentofQuestions++;
        }
        else if(val == -1)
        {
            if(0 >= currentofQuestions)
            {
                return;
            }
            currentofQuestions--;
        }

        $("#word-id").attr("value",ansPaper[currentofQuestions]);
        $("#show-word").text(question_words[currentofQuestions]);



        for(i=0; i<4; i++)
        {
            $("#radio"+i).attr("value",question_options[currentofQuestions][i][1]);
            $("#show-option"+i).text(question_options[currentofQuestions][i][0]);
        }

    }

    function checkSelectedAns()
    {
        if(savedAns[currentofQuestions] != 0)
        {
            check = ""+savedAns[currentofQuestions];
            $("input[value="+check+"]").prop('checked', true);
        }
        else
        {
            $("input[name=radioans]:checked").prop('checked', false);
        }
    }





</script>
@endsection('scripts')





