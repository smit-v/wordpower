@extends('layouts.app')
@section('content')

      {{-- Index --}}
      @if($errors->any())
            <div class="mx-auto" style="width: 400px;">
                @foreach ($errors->all() as $error)
                <p class="text-danger m-0"> {{ $error }}</p>
            @endforeach
            </div>
        @endif
      <div class="range d-flex justify-content-center mt-3">
       <div>
        <form class="form-inline" action="{{ route('words.rangeCheck') }}" method="POST">
            @csrf
            <div class="form-group mx-sm-3 mb-2">
                <input type="number" class="form-control" id="from" name='From-field' placeholder="From" value="{{ old('From-field') }}">
              </div>
            <div class="form-group mx-sm-3 mb-2">
              <input type="number" class="form-control" id="to" name='To-field' placeholder="To" value="{{ old('To-field') }}">
            </div>
            <button type="submit" class="btn btn-primary mb-2">Search</button>
          </form>
       </div>
     </div>



      <div class="row">
        @foreach ($words as $word)
        <div class="col-sm-6 mt-3">
            <div class="card p-0">
                <div class="card-header bg-dark text-light">
                  <p class="font-weight-bold m-0">Word {{ $word->id }}</p>
                </div>
                <div class="card-body">
                  <h2 class="card-title text-capitalize">{{ $word->name }}</h2>
                  <p class="card-text text-capitalize">{{ $word->meaning }}</p>
                  <a href="{{ route('words.edit', $word->id) }}" class="btn btn-warning">Edit</a>
                </div>
              </div>
        </div>
        @endforeach
      </div>
@endsection
@section('footer-val','mt-3')
