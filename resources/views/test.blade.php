@extends('layouts.app')
@section('content')


    <div class="d-flex justify-content-center">
        <div class="card mt-5 w-75">
            <div class="card-header bg-dark text-light">
              <p class="font-weight-bold m-0">MCQs</p>
            </div>
            <div class="card-body">


                    <fieldset class="form-group">
                      <div class="row">
                        <div class="col-md-5">
                            <input id="word-id" type="hidden" value="{{ $word[1] }}">
                            <h1 class="text-capitalize">{{ $word[0] }}</h1>
                        </div>
                        <div class="col-sm-7">
                          @foreach ($meanings as $meaning)
                          <div class="form-check">
                            <input id="radio{{$loop->index}}" class="form-check-input" type="radio" name="radioans" value="{{ $meaning[1] }}">
                            <label class="form-check-label" for="radio{{$loop->index}}">

                              <p class="text-capitalize">{{ $meaning[0] }}</p>
                            </label>
                          </div>
                          @endforeach
                        </div>
                      </div>
                    </fieldset>

                    <div class="form-group row">
                      <div class="col-sm-8 d-flex justify-content-center">
                        <button id="btn-submit" class="btn btn-primary m-0">Submit</button>
                        <a href="{{ route('words.test',$checks) }}" class="btn btn-secondary ml-3">Next</a>
                      </div>
                      <div class="col-sm-4">
                        <p id="ans" class="m-0"></p>
                      </div>
                    </div>


            </div>
          </div>
    </div>
@endsection

@section('footer-val','fixed-bottom')
@section('scripts')
<script>
    $('#btn-submit').click(function(){
        var ans = $("input[name='radioans']:checked").val();
        if(ans == null)
        {
            alert('Please select an option');
            return;
        }
        var word = $('#word-id').val();
        if(ans == word)
        {
            $('#ans').text("Yes!! Its Correct!!");
            $('#ans').removeClass("text-danger");
            $('#ans').addClass("text-success");
        }
        else
        {
            $('#ans').text("Oh No! its wrong!");
            $('#ans').removeClass("text-success");
            $('#ans').addClass("text-danger");
        }
    });
</script>
@endsection
