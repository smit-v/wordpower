<?php

use App\Http\Controllers\WordController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Route::resource('words', WordController::class);
// Route::get('/',[WordController::class, 'index'])->name('words.index');
Route::post('/range',[WordController::class, 'rangeCheck'])->name('words.rangeCheck');


Route::post('/setRange',[WordController::class, 'setRange'])->name('words.test.setRange');
Route::get('/test/{from}/{to}',[WordController::class, 'makeTestQuestion'])->name('words.test');
Route::get('/check/{from}/{to}',[WordController::class, 'checkWord'])->name('words.check');
Route::get('/mcq/{from}/{to}',[WordController::class, 'MCQ'])->name('words.mcq');
Route::get('/mcqPage/{from}/{to}',[WordController::class, 'MCQpage'])->name('words.mcqPage');
